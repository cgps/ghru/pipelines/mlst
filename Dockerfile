FROM continuumio/miniconda3
LABEL authors="Anthony Underwood" \
      description="Docker image containing requirements for the GHRU MLST pipeline"
RUN apt update

## Install ariba via conda
COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a
ENV PATH /opt/conda/envs/ghru-mlst/bin:$PATH

## Get ariba mlst databases
RUN ariba pubmlstget "Acinetobacter baumannii#1"  acinetobacter_baumannii
RUN ariba pubmlstget "Enterobacter cloacae"  enterobacter_cloacae
RUN ariba pubmlstget "Enterococcus faecalis"  enterococcus_faecalis
RUN ariba pubmlstget "Enterococcus faecium"  enterococcus_faecium
RUN ariba pubmlstget "Escherichia coli#1"  escherichia_coli
RUN ariba pubmlstget "Klebsiella pneumoniae"  klebsiella_pneumoniae
RUN ariba pubmlstget "Neisseria spp."  neisseria_spp.
RUN ariba pubmlstget "Pseudomonas aeruginosa"  pseudomonas_aeruginosa
RUN ariba pubmlstget "Salmonella enterica"  salmonella_enterica
RUN ariba pubmlstget "Staphylococcus aureus"  staphylococcus_aureus
RUN ariba pubmlstget "Staphylococcus haemolyticus"  staphylococcus_haemolyticus
RUN ariba pubmlstget "Streptococcus pneumoniae"  streptococcus_pneumoniae
RUN ariba pubmlstget "Vibrio cholerae"  vibrio_cholerae

# install procps which is required by Nextflow trace
RUN apt install -y procps