#!/usr/bin/env nextflow
/*

========================================================================================
                          GHRU MLST Pipeline
========================================================================================
*/

// Pipeline version
version = '1.0'

def versionMessage(){
  log.info"""
  ==============================================
   MLST Pipeline version ${version}
  ==============================================
  """.stripIndent()
}
def helpMessage() {
    log.info"""
    Mandatory arguments:
      --input_dir      Path to input dir. This must be used in conjunction with fastq_pattern
      --fastq_pattern  The regular expression that will match fastq files e.g '*_{1,2}.fastq.gz'
      --output_dir     Path to output dir
      --species        The species of the sample. This must be one of
                       Acinetobacter baumannii
                       Enterobacter cloacae
                       Enterococcus faecalis
                       Enterococcus faecium
                       Escherichia coli
                       Klebsiella pneumoniae
                       Neisseria spp.
                       Pseudomonas aeruginosa
                       Salmonella enterica
                       Staphylococcus aureus
                       Staphylococcus haemolyticus
                       Streptococcus pneumoniae
                       Vibrio cholerae
    """.stripIndent()
   }


//  print help if required
params.help = false
// Show help message
if (params.help){
    versionMessage()
    helpMessage()
    exit 0
}

// Show version number
params.version = false
if (params.version){
    versionMessage()
    exit 0
}
/***************** Setup inputs and channels ************************/
params.input_dir = false
params.fastq_pattern = false
params.output_dir = false
params.species = false

// set up input directory
input_dir = Helper.check_mandatory_parameter(params, 'input_dir') - ~/\/$/

//  check a pattern has been specified
fastq_pattern = Helper.check_mandatory_parameter(params, 'fastq_pattern')

//
read_path=input_dir + "/" + fastq_pattern

// set up output directorypointfinder_db
output_dir = Helper.check_mandatory_parameter(params, 'output_dir') - ~/\/$/

// set up species
species = Helper.check_mandatory_parameter(params, 'species')
// check species is valid
Helper.check_parameter_value('species', params.species, ['Acinetobacter baumannii', 'Enterobacter cloacae', 'Enterococcus faecalis', 'Enterococcus faecium', 'Escherichia coli', 'Klebsiella pneumoniae', 'Neisseria spp.', 'Pseudomonas aeruginosa', 'Salmonella enterica', 'Staphylococcus aureus', 'Staphylococcus haemolyticus', 'Streptococcus pneumoniae', 'Vibrio cholerae'])
// make mlst db name from species
species_db_name = species.replaceFirst(/ /, '_')
species_db_name = species_db_name.toLowerCase()



//Setup input Channel from Read path
Channel
    .fromFilePairs( read_path )
    .ifEmpty { error "Cannot find any reads matching: ${read_path}" }
    .set { raw_fastqs }

process run_ariba {
  tag { sample_id }
  publishDir "${output_dir}/individual_reports",
  mode:"copy"

  input:
  set sample_id, file(reads) from raw_fastqs

  output:
  file("${sample_id}_mlst_report.tsv") into mlst_reports
  file("${sample_id}_mlst_report.details.tsv")

  script:

  """
  ariba run /${species_db_name}/ref_db ${reads[0]} ${reads[1]} ${sample_id}
  cp  ${sample_id}/mlst_report.tsv ${sample_id}_mlst_report.tsv
  cp  ${sample_id}/mlst_report.details.tsv ${sample_id}_mlst_report.details.tsv
  """
}

process combine_mlst_reports {
  tag { 'combine mlst reports'}
  publishDir "${output_dir}",
  mode:"copy"

  input:
  file(mlst_report_files) from mlst_reports.collect( sort: {a, b -> a.getBaseName() <=> b.getBaseName()} )

  output:
  file("combined_mlst_report.tsv")

  script:
  """
  MLST_REPORT_FILES=(${mlst_report_files})
  for index in \${!MLST_REPORT_FILES[@]}; do
    MLST_REPORT_FILE=\${MLST_REPORT_FILES[\$index]}
    SAMPLE_ID=\${MLST_REPORT_FILE%_mlst_report.tsv}
    # add header line if first file
    if [[ \$index -eq 0 ]]; then
      echo "Sample ID\t\$(head -1 \${MLST_REPORT_FILE})" >> combined_mlst_report.tsv
    fi
    echo "\${SAMPLE_ID}\t\$(awk 'FNR==2 {print}' \${MLST_REPORT_FILE})" >> combined_mlst_report.tsv
  done
  """
}
